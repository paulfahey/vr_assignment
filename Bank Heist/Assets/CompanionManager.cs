﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompanionManager : MonoBehaviour
{

    public GameObject lightsManager;
    public Image blueprintMain;
    public Image background;
    public int blueprintIndex;

    public Vault thisVault;
    public GameObject vaultOBJ;

    public Camera[] cameras;
    public int cameraIndex;
    bool camerasOn;


    // Use this for initialization
    void Start()
    {
        //lightsManager.SetActive(false)
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartHack();
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            CamerasOn();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            if (camerasOn)
            {
                SwitchCamera(0);
            }
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (camerasOn)
            {
                SwitchCamera(1);
            }
        }
    }

    public void StartHack()
    {
        lightsManager.GetComponent<PanelManager>().vaultType = blueprintIndex;
        lightsManager.GetComponent<PanelManager>().SetPassCode();
        lightsManager.GetComponent<PanelManager>().hasStarted = true;
        StartCoroutine(lightsManager.GetComponent<PanelManager>().spawnLight());
        blueprintMain.gameObject.SetActive(false);
        background.sprite = Resources.Load<Sprite>("Hack_Screen");
    }

    public void FailHack()
    {
        blueprintMain.gameObject.SetActive(true);
        background.sprite = Resources.Load<Sprite>("Background");
    }

    public void HackScreen()
    {
        GetComponent<Canvas>().enabled = true;
    }

    public void CamerasOn()
    {
        if (!camerasOn)
        {
            if (!lightsManager.GetComponent<PanelManager>().hasStarted)
            {
                background.gameObject.SetActive(false);
                blueprintMain.gameObject.SetActive(false);
                camerasOn = true;
            }
            else
            {
                background.gameObject.SetActive(false);
                camerasOn = true;
            }
        }
        else
        {
            if (!lightsManager.GetComponent<PanelManager>().hasStarted)
            {
                background.gameObject.SetActive(true);
                blueprintMain.gameObject.SetActive(true);
                camerasOn = false;
            }
            else
            {
                background.gameObject.SetActive(true);
                camerasOn = false;
            }
        }
    }


    void SwitchCamera(int index)
    {
        if (index == 0)
        {
            if (cameraIndex < cameras.Length - 1)
            {
                cameras[cameraIndex].gameObject.SetActive(false);
                cameraIndex++;
                cameras[cameraIndex].gameObject.SetActive(true);
            }
        }
        else if (index == 1)
        {
            if (cameraIndex > 0)
            {
                cameras[cameraIndex].gameObject.SetActive(false);
                cameraIndex--;
                cameras[cameraIndex].gameObject.SetActive(true);
            }
        }
    }

    public void SwitchPrintLeft()
    {
        if(blueprintIndex > 0)
        {
            blueprintIndex--;
            blueprintMain.GetComponent<Image>().sprite = Resources.Load<Sprite>("blueprint_" + blueprintIndex);
        }
       
    }

    public void SwitchPrintRight()
    {
        if (blueprintIndex < 2)
        {
            blueprintIndex++;
            blueprintMain.GetComponent<Image>().sprite = Resources.Load<Sprite>("blueprint_" + blueprintIndex);
        }
    }

}
