﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleButton : MonoBehaviour {

    public int buttonIndex;
    public Vault thisVault;

    // Use this for initialization
    void Start()
    {
        thisVault = GameObject.Find("Main Console").GetComponent<Vault>();
        Activate();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Activate()
    {
        GetComponent<MeshRenderer>().material.shader = Shader.Find("Sprites/Diffuse");
		Invoke("DeActivate", 1f);
    }

    public void DeActivate()
    {
        GetComponent<MeshRenderer>().material.shader = Shader.Find("Standard");
    }

    public void Press()
    {
        Activate();
        thisVault.EnterCodeInt(buttonIndex);
    }
}
