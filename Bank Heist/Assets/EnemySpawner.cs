﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour {

    public GameObject enemyPrefab;
    public Transform[] enemySpawnPoints;

    // Use this for initialization
    public override void OnStartServer()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update () {
		
	}

    
    public void Spawn()
    {
        for (int i = 0; i < 2; i++)
        {
            GameObject enemyClone = Instantiate(enemyPrefab, enemySpawnPoints[i].position, Quaternion.identity);
            NetworkServer.Spawn(enemyClone);
        }
    }
}
