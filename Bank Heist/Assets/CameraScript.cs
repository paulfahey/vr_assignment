﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public Vector3 pos;
    public Vector3 rot;

	// Use this for initialization
	void Start () {
        Invoke("MoveCam", 2);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void MoveCam()
    {
        Debug.Log("moving");
        transform.position = pos;
        transform.rotation = Quaternion.Euler(rot);
    }
}
