﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager _gameManager;

    public bool canPress;

    public GameObject[] enemies;

    // Use this for initialization
    void Start()
    {
        if (_gameManager == null)
        {
            _gameManager = this;
        }
        if (_gameManager != this)
        {
            Destroy(_gameManager);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Alarm()
    {
        foreach (GameObject enemy in enemies)
        {
            enemy.GetComponent<Enemy>().Hunt();
        }
    }

    public void EndGame()
    {
        Debug.Log("Game Over!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
