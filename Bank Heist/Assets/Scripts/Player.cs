﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour 
{
	
	//REFERENCE TO ENEMY SCRIPT
	Enemy enemyScript;
	public bool isCaught;
	GameObject gmObj;
	private GameManager gm;

	// Use this for initialization
	void Start () 
	{
		gmObj = GameObject.Find("GameManager");
		gm = gmObj.GetComponent<GameManager>();
	}

	// Update is called once per frame
	void Update ()
	{

		//CONDITION FOR GAME OVER
		if (isCaught == true) 
		{
			GotCaught ();
		}

	}


	void OnTriggerEnter (Collider other)
	{

		if (other.tag == "Enemy")
		{
			isCaught = true;
		}
	}

	//FUNCTION FOR GAME OVER
	void GotCaught()
	{
//		print ("Game Over!");
		Time.timeScale = 0;
		gm.EndGame();

	}

}
