﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelButton : MonoBehaviour {

    public int ButtonIndex; 

	// Use this for initialization
	void Start () {
        DeActivate();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Activate()
    {
        GetComponent<Image>().material = null;
    }

    public void DeActivate()
    {
        GetComponent<Image>().material = Resources.Load<Material>("Diffuse");
    }

    public void Press()
    {
        Activate();
    }
}
