﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Vault : MonoBehaviour
{

    public static Vault thisVault;

    public enum VaultType { Type1, Type2 };
    public VaultType thisType;
    public int[][] possibleCodes;
    public int[] thisCode;
    public GameObject[] panelButtons;
    public GameObject safe;
    int codeIndex = 0;

    public bool canPress = false;
    public int failIndex = 0;

    AudioSource aud;



    private void Awake()
    {
        thisVault = GetComponent<Vault>();
        possibleCodes = new int[4][];
        possibleCodes[0] = new int[] { 1, 3, 2, 1, 7, 5, 0 };
        possibleCodes[1] = new int[] { 1, 2, 0, 3, 6, 8, 7 };
        possibleCodes[2] = new int[] { 0, 2, 1, 0, 5, 3, 1 };
        possibleCodes[3] = new int[] { 0, 1, 2, 2, 7, 3, 0 };
        int roll = Random.Range(0, 8);
        
        if (thisType == VaultType.Type1)
        {
            thisCode = possibleCodes[0];
        }
        else
        {
            thisCode = possibleCodes[1];
        }
    }
    // Use this for initialization
    void Start()
    {
        Debug.Log(thisCode.Length);
    }


    public void OpenVault()
    {
        Destroy(safe);
    }

    public void Fail()
    {
        
    }

    public void EnterCodeInt(int panelIndex)
    {
        if (panelIndex == thisCode[codeIndex])
        {
            codeIndex++;
            if (codeIndex >= thisCode.Length)
            {
                Debug.Log("Finishing");
                Destroy(safe);
            }
            else
            {
                //Debug.Log(thisCode[codeIndex+1]);
                Debug.Log("Correct");
            }
        }
        else
        {
            failIndex++;
            foreach (GameObject button in panelButtons)
            {
                button.GetComponent<ConsoleButton>().DeActivate();
            }
            if (failIndex < 3)
            {
                codeIndex = 0;
                Debug.Log("Incorrect");
            }
            else
            {
                Debug.Log("Failure!");
                GameManager._gameManager.Alarm();
            }
          
            
        }
    }
}
