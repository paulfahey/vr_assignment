﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRClick : MonoBehaviour 
{
	public Material on;
	public Material off;
	Renderer r;

	void Start () {

		r = this.GetComponent<Renderer> ();
		r.material = off;
	}

	public void ColourChange()
	{
		this.GetComponent<Renderer> ().material = on;
	}

}
