﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour 
{
	//VARIABLES TO CONTROL THE ENEMY PATROLLING
	public GameObject waypointManagerObj;
	public GameObject playerObj;
	private WaypointManager waypointManagerr;
	private GameObject destination;
	public const float proxyDist = 0.5f;
	public const float decelerationFactor = 5f;
	Vector3 source;
	Vector3 target;
	Vector3 outputVelocity;
	Vector3 directionToDestination;
	Vector3 velocityToDestination;
	float distanceToDestination;
	float speed;
	float huntSpeed = 15f;
	BoxCollider enemyCol;

	NavMeshAgent navAgent;
	public Transform player;


	//SETTING UP THE STATES OF THE ENEMY
	enum States
	{
		Patrol,
		Hunt
	}

	//SET THE CURRENT STATE OF THE ENEMY
	States currentState = States.Patrol;


    // Use this for initialization
    void Awake()
    {
        //GETTING REFERENCES TO COMPONENTS OF THE ENEMY
        waypointManagerObj = GameObject.Find("WaypointManager");
        waypointManagerr = waypointManagerObj.GetComponent<WaypointManager>();
        destination = waypointManagerr.NextWaypoint(null);

        GetComponent<Transform>().LookAt(destination.transform);
        navAgent = GetComponent<NavMeshAgent>();

		enemyCol = this.GetComponent<BoxCollider> ();
		enemyCol.enabled = false;
    }

    private void Update()
    {
        GameObject thePlayer = GameObject.Find("Player(VR)");
        if(thePlayer != null)
        {
            playerObj = thePlayer;
            player = playerObj.transform;
        }
        
    }

    // Update is called once per frame
    void FixedUpdate () 
	{
		//SET UP THE MOVEMENT AND RAYCASTING
		source = transform.position;
		RaycastHit hit;
		Ray enemyRayCastR = new Ray (transform.position + Vector3.right, transform.right);
		Ray enemyRayCastL = new Ray (transform.position + Vector3.left, transform.forward);
		Debug.DrawRay (transform.position + Vector3.right, transform.right ,Color.blue);
		Debug.DrawRay (transform.position + Vector3.left, transform.forward ,Color.red);


		GetComponent<Transform> ().LookAt (destination.transform);

		switch (currentState) 
		{
		case States.Patrol:
			Patrol ();
			break;
		case States.Hunt:
			Hunt (); 
			break;

		}

		//IF THE ENEMY SEES THE PLAYER
		if (Physics.Raycast (enemyRayCastR, out hit, 50)) {
			if (hit.collider.tag == "Player") 
			{
//				Debug.Log ("THIEF!"); 

				currentState = States.Hunt;
			}
		} 
			
	}

	//FUNCTION FOR THE ENEMY TO TRAVEL TO EACH POINT
	void Patrol()
	{
		navAgent.SetDestination (target);
		target = destination.transform.position;
		outputVelocity = Arrive (source, target);
		GetComponent<Rigidbody> ().AddForce (outputVelocity, ForceMode.VelocityChange);

		if (Vector3.Distance(source, target) < proxyDist)
		{
			destination = waypointManagerr.NextWaypoint (destination);
		}
	}

	//WHEN THE ENEMY IS ARRIVING CLOSE TO THE POINT IT IS CURRENTLY TRAVELLING TO
	private Vector3 Arrive (Vector3 source, Vector3 target)
	{
		distanceToDestination = Vector3.Distance (source, target);
		directionToDestination = Vector3.Normalize (target - source);
		speed = distanceToDestination / decelerationFactor;
		velocityToDestination = speed * directionToDestination;
		return velocityToDestination - GetComponent<Rigidbody> ().velocity;
	}

	//FUNCTION FOR THE ENEMY TO CHASE THE PLAYER
	public void Hunt()
	{
		GetComponent<Rigidbody> ().velocity = Vector3.zero;
		outputVelocity = Vector3.forward;
		destination = playerObj;
		navAgent.SetDestination (player.position);

		enemyCol.enabled = true;
	}

	//FUNCTION TO END THE GAME IF THE PLAYER GETS CAUGHT BY THE ENEMY
	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Player") 
		{
//			playerScript.isCaught = true;
			Debug.Log ("BUSTED SUCKAH!");
            GameManager._gameManager.EndGame();
		}
	}
}
