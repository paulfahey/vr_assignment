﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CompanionPlayerNetwork : NetworkBehaviour {

    public Canvas companionCanvas;

    public Camera hostCam;

    public Camera[] cameras;

    private void Awake()
    {
        if (companionCanvas == null)
        {
            companionCanvas = GameObject.Find("CompanionCanvas").GetComponent<Canvas>();
        }

        cameras = GameObject.Find("Security Cameras").GetComponentsInChildren<Camera>();
    }

    // Use this for initialization
    void Start () {
       
        if (!isServer)
        {
            companionCanvas.enabled = true;
            GetComponent<Camera>().enabled = false;
            this.gameObject.name = "Player(Mobile)";
        }
        else
        {
            companionCanvas.enabled = false;
            foreach(Camera camera in cameras)
            {
                camera.enabled = false;
            }
            this.gameObject.name = "Player(VR)";
            Destroy(this);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
