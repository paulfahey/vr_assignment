﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointManager : MonoBehaviour 
{
	//VARIABLES TO SET UP THE WAYPOINTS
	int nextPoint;
	public GameObject[] waypoints;

	public GameObject NextWaypoint (GameObject current)
	{
		//GOES THROUGH EACH WAYPOINT UNTIL IT HITS THE MAX AMOUNT OF WAYPOINTS
		if (current != null) {
			for (int i = 0; i < waypoints.Length; i++) {
				if (current == waypoints [i]) {
					nextPoint = (i + 1) % waypoints.Length;
				}
			}
		} else 
			//RETURN TO ORIGINAL WAYPOINT
		{
			nextPoint = 0;
		}
		return waypoints [nextPoint];
	}

}
