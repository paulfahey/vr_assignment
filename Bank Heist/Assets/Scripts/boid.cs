﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boid : MonoBehaviour 
{
	public Vector3 force;
	public Vector3 velocity;
	public Vector3 accelleration;
	float mass = 1f;
	float maxForce = 10f;
	float maxSpeed = 10f;
	public GameObject target;
	public float slowingDistance = 10;

	// Use this for initialization
	void Start () 
	{

	}

	public void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawLine (transform.position, transform.position + (force * 10));
	}

	Vector3 SeekForce()
	{
		Vector3 toTarget = target.transform.position - transform.position;
		toTarget.Normalize ();
		Vector3 desired = toTarget * maxSpeed;
		return desired - velocity;
	}

	Vector3 Calculate()
	{
		return ArriveForce();
	}

	Vector3 ArriveForce()
	{
		Vector3 toTarget = target.transform.position - transform.position;
		float dist = toTarget.magnitude;
		float ramped = maxSpeed * (dist / slowingDistance);
		float clamped = Mathf.Min (ramped, maxSpeed);
		Vector3 desired = clamped * (toTarget / dist);
		return desired - velocity;
	}

	void Update () 
	{
		force = Calculate ();
		force = Vector3.ClampMagnitude (force,maxForce);
		accelleration = force / mass;
		velocity += accelleration * Time.deltaTime;
		velocity = Vector3.ClampMagnitude (velocity, maxSpeed);
		velocity *= 0.99f;
		transform.position += velocity * Time.deltaTime;

		if (velocity.magnitude > float.Epsilon) 
		{
			transform.forward = velocity;
		}
	}
}