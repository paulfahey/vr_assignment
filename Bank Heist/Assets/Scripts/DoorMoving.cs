﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMoving : MonoBehaviour {

	public Transform endPos;
	private Vector3 start;
	private Vector3 end;
	private float secondsTillComplete = 2f;

	public AudioSource beep;
	public AudioSource boop;

	// Use this for initialization
	void Start () 
	{
		start = transform.position;
		end = endPos.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	public void Descend()
	{
		print ("BEEP!");
		transform.position = Vector3.Lerp (start, end, secondsTillComplete);
		InvokeRepeating ("Beeping", 0, 1);
	}

	public void Ascend()
	{
		print ("BOOP!");
		CancelInvoke ("Beeping");
		Invoke ("Booping",0);
		transform.position = Vector3.Lerp (end, start, secondsTillComplete);
	}

	public void Beeping()
	{
		beep.Play ();
	}

	public void Booping()
	{
		boop.Play ();
	}
}
