﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClick : MonoBehaviour {

	public Material on;
	public Material off;
	Renderer r;
	public DoorMoving dm;


	// Use this for initialization
	public void Start () {

		r = this.GetComponent<Renderer> ();
		r.material = off;
	}
	
	// Update is called once per frame
	void Update () 
	{


	}

	public void ButtonClick ()
	{
		this.GetComponent<Renderer> ().material = on;

		dm.Descend ();
//		door.transform.position = waypoint.transform.position;;
		Invoke ("TurnOff", 5);
	}

	void TurnOff()
	{
		dm.Ascend ();
		r.material = off;

	}
}
