﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Light_Movement : MonoBehaviour
{
    Vault thisVault;
    CompanionManager co_Manager;

    private void Awake()
    {
        thisVault = GameObject.Find("Main Console").GetComponent<Vault>();
    }

    // Use this for initialization
    void Start()
    {
        transform.position = new Vector2(transform.position.x, transform.parent.position.y);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.Lerp(transform.position, new Vector2(2200, transform.parent.position.y), 0.1f * Time.deltaTime);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Scanner")
        {

            thisVault.canPress = true;
            Debug.Log("Found");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Scanner")
        {
            thisVault.canPress = false;
            Debug.Log("Left");
        }
    }


}
