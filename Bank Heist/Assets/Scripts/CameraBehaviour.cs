﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public Camera[] cameras;
    public int cameraIndex;

    // Use this for initialization
    void Start()
    {
        foreach(Camera camera in cameras)
        {
            camera.gameObject.SetActive(false);
        }
        cameras[0].gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            SwitchCamera(0);
        }
        if(Input.GetKeyDown(KeyCode.A))
        {
            SwitchCamera(1);
        }
    }

    void SwitchCamera(int index)
    {
        if (index == 0)
        {
            if (cameraIndex < cameras.Length - 1)
            {
                cameras[cameraIndex].gameObject.SetActive(false);
                cameraIndex++;
                cameras[cameraIndex].gameObject.SetActive(true);
            }
        }
        else if (index == 1)
        {
            if (cameraIndex > 0)
            {
                cameras[cameraIndex].gameObject.SetActive(false);
                cameraIndex--;
                cameras[cameraIndex].gameObject.SetActive(true);
            }
        }
    }
}
