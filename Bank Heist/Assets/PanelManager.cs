﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class PanelManager : MonoBehaviour {


    public GameObject light;
    public int[] passcode;
    public Vault thisVault;

    public bool hasStarted;

    public CompanionManager c_Manager;

    public int vaultType;

    public Image hackText;

    private void Awake()
    {
        thisVault = GameObject.Find("Main Console").GetComponent<Vault>();
    }

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    public void SetPassCode()
    {
        if (vaultType == 1)
        {
            passcode = new int[] { 1, 3, 2, 1, 7, 5, 0 };
        }
        if (vaultType == 2)
        {
            passcode = new int[] { 1, 2, 0, 3, 6, 8, 7 };
        }
    }

    public IEnumerator spawnLight()
    {
        int i = 0;
        hackText.enabled = true;
        yield return new WaitForSeconds(2);
        hackText.enabled = false;
        while (hasStarted)
        {
            Debug.Log("Starting...");
            if (i < passcode.Length)
            {
                int lightNum = passcode[i];
                GameObject lightClone = (GameObject)Instantiate(light, new Vector2(this.transform.position.x,transform.position.y), Quaternion.identity);
                lightClone.transform.SetParent(this.transform);
                lightClone.GetComponent<Image>().sprite = Resources.Load<Sprite>("Buttons/Color_"+lightNum);
                yield return new WaitForSeconds(Random.Range(0.5f,2));
                i++;
            }
            else
            {
                Debug.Log("Blah");
                hasStarted = false;
                break;
            }
        }
    }

}
